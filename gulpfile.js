/*-------------- PATH Variables --------------*/
var stylespath = './styles',
	scriptspath = './scripts',
	pagespath = './pages';
	
var stylessrc = './styles/sass/main.scss', 
	scriptssrc = './app/**/*.js',
	pagessrc = './app/*.njk';
	nunjucksWatch = ['./app/**/*.njk', './templates/**/*.njk'];
/*-------------- GULP Dependencies -------------*/
var gulp = require('gulp'),
	sass = require('gulp-sass'),
	prefix = require('gulp-autoprefixer'),
	minifyCSS = require('gulp-minify-css'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	order = require('gulp-order'),
	plumber = require('gulp-plumber'),
	size = require('gulp-size'),
	jshint = require('gulp-jshint'),
	watch = require('gulp-watch'),
	gutil = require('gulp-util'),
	nunjuckscompile = require('gulp-nunjucks'),
	nunjucksrender = require('gulp-nunjucks-render');
/*---------------------------------------------*/

gulp.task('index', function(){
	gulp.src(['./app/main/*.njk'])
	.pipe(plumber())
	.pipe(nunjucksrender({
		path: ['./templates']
	}))
	.pipe(gulp.dest(''));
});

gulp.task('pages', function(){
	gulp.src([pagessrc])
	.pipe(plumber())
	.pipe(nunjucksrender({
		path: ['./templates']
	}))
	.pipe(gulp.dest(pagespath));
});

gulp.task('styles', function(){
	gulp.src([stylessrc])
	.pipe(plumber())
	.pipe(sass())
	.pipe(prefix('last 2 versions', 'ie 9', 'ie 8', 'ie 7'))
	.pipe(gulp.dest(stylespath))
	.pipe(minifyCSS())
	.pipe(rename({suffix: '.min'}))
	.pipe(size(({showFiles: true, title: 'main'})))
	.pipe(gulp.dest(stylespath));
});

gulp.task('app', function(){
	gulp.src(scriptssrc)
	.pipe(order([
		'index.js',
		'routes.js',
		'main/main.controller.js'
	]))
	.pipe(plumber())
	.pipe(concat('app.js'))
	.pipe(rename({ suffix:'.min' }))
	.pipe(uglify())
	.pipe(gulp.dest(scriptspath));
});



gulp.task('scripts', [
	'pages',
	'styles', 
	'app'
	]);

gulp.task('watch', function() {
	gulp.watch([
		nunjucksWatch,
		'./styles/**/*.scss',
		scriptssrc
	],
	[
		'index',
		'pages',
		'styles',
		'app'
	]);
});

gulp.task('default', [
	'index',
	'pages',
	'styles', 
	'scripts',
	'watch'
	]);