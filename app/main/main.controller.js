'use strict';

var ameApp = angular.module('main.controller', []);

ameApp
	.controller('MainCtrl', [
		'$scope',
		'$route',
		function ($scope, $route){
			$scope.title='test angular controller';
		}
	]);